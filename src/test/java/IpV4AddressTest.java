import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IpV4AddressTest {
    @Test
    void t1() {
        var ip = IpV4Address.ngaStringuMeSlash("123.456.789.112/14");

        assertArrayEquals(ip.adresaIp, new int[]{123, 456, 789, 112});
        assertEquals(ip.networkSize, 14);
        assertEquals(ip.subnetSize, 32 - 14);
    }

    @Test
    void t2() {
        assertArrayEquals(
                IpV4Address.llogaritNetworkMask(32 - 24),
                new int[] {255, 255, 255, 0}
        );

        assertArrayEquals(
                IpV4Address.llogaritNetworkMask(32 - 23),
                new int[] {255, 255, 254, 0}
        );

        assertArrayEquals(
                IpV4Address.llogaritNetworkMask(32 - 22),
                new int[] {255, 255, 252, 0}
        );

        assertArrayEquals(
                IpV4Address.llogaritNetworkMask(32 - 4),
                new int[] {240, 0, 0, 0}
        );

        assertArrayEquals(
                IpV4Address.llogaritNetworkMask(32 - 15),
                new int[] {255, 254, 0, 0}
        );
    }

    @Test
    void t3() {
        var ip1 = IpV4Address.ngaStringuMeSlash("192.168.100.5/22");
        assertArrayEquals(
                ip1.networkId,
                new int[]{192, 168, 100, 0}
        );
        assertArrayEquals(
                ip1.subnetMask,
                new int[]{255, 255, 252, 0}
        );
        assertArrayEquals(
                ip1.adresaPare,
                new int[]{192, 168, 100, 1}
        );
        assertArrayEquals(
                ip1.broadcast,
                new int[]{192, 168, 103, 255}
        );
        assertArrayEquals(
                ip1.adresaFundit,
                new int[]{192, 168, 103, 254}
        );
        assertEquals(ip1.nrHostesh, 1022);

        var ip2 = IpV4Address.ngaStringuMeSlash("192.168.100.72/20");
        assertArrayEquals(
                ip2.subnetMask,
                new int[]{255, 255, 240, 0}
        );
        assertArrayEquals(
                ip2.networkId,
                new int[]{192, 168, 100 & 240, 0}
        );
        assertArrayEquals(
                ip2.adresaPare,
                new int[]{192, 168, 96, 1}
        );
        assertArrayEquals(
                ip2.broadcast,
                new int[]{192, 168, 111, 255}
        );
        assertArrayEquals(
                ip2.adresaFundit,
                new int[]{192, 168, 111, 254}
        );
        assertEquals(ip2.nrHostesh, 4094);

        var ip3 = IpV4Address.ngaStringuMeSlash("192.168.100.255/18");
        assertArrayEquals(
                ip3.subnetMask,
                new int[]{255, 255,192, 0}
        );
        assertArrayEquals(
                ip3.networkId,
                new int[]{192, 168, 64, 0}
        );
        assertArrayEquals(
                ip3.adresaPare,
                new int[]{192, 168, 64, 1}
        );
        assertArrayEquals(
                ip3.broadcast,
                new int[]{192, 168, 127, 255}
        );
        assertArrayEquals(
                ip3.adresaFundit,
                new int[]{192, 168, 127, 254}
        );
        assertEquals(ip3.nrHostesh, 16382);

        var ip4 = IpV4Address.ngaStringuMeSlash("37.48.129.46/26");
        assertArrayEquals(
                ip4.subnetMask,
                new int[]{255, 255, 255, 192}
        );
        assertArrayEquals(
                ip4.networkId,
                new int[]{37, 48, 129, 0}
        );
        assertArrayEquals(
                ip4.adresaPare,
                new int[]{37, 48, 129, 1}
        );
        assertArrayEquals(
                ip4.broadcast,
                new int[]{37, 48, 129, 63}
        );
        assertArrayEquals(
                ip4.adresaFundit,
                new int[]{37, 48, 129, 62}
        );
        assertEquals(ip4.nrHostesh, 62);
    }

    @Test
    void test4() {
        var min = IpV4Address.formatBinar(new int[]{172, 19, 40, 1});
        var max = IpV4Address.formatBinar(new int[]{172, 19, 47, 254});

        assertEquals(min, 2886936577L);
        assertEquals(max, 2886938622L);
        assertEquals(2886938622L - 2886936577L + 1, 2046);
    }
}
