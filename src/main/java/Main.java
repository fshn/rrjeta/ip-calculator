import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		var stdin = new Scanner(System.in);

		System.out.println("[Bosh ndalon]\n");

		while (true) {
			System.out.print("Vendos IP (xxx.xxx.xxx.xxx/yy): ");
			var str = stdin.nextLine().trim();
			if (str.isBlank()) {
				break;
			}
			var ip = IpV4Address.ngaStringuMeSlash(str);
			System.out.println(ip.raporti());
		}
	}
}
