import java.util.Arrays;

public final class IpV4Address {
	private static final int KUARTETE = 4;
	private static final int BITE_NE_BYTE = 8;
	private static final int NR_MAGJIK = 0xFF;

	public final int adresaIp[];
	public final int networkSize;
	public final int subnetSize;
	public final int subnetMask[];
	public final int networkId[];
	public final int adresaPare[];
	public final int broadcast[];
	public final int adresaFundit[];
	public final long nrHostesh;

	private IpV4Address(int[] adresaIp, int networkSize, int subnetSize) {
		this.adresaIp = adresaIp;
		this.networkSize = networkSize;
		this.subnetSize = subnetSize;
		this.subnetMask = llogaritNetworkMask(subnetSize);
		this.networkId = llogaritNetworkId();
		this.adresaPare = llogaritAdresaPare();
		this.broadcast = llogaritBroadcast();
		this.adresaFundit = llogaritAdresaFundit();
		this.nrHostesh = llogaritNrHostesh();
	}

	public static IpV4Address ngaStringuMeSlash(String str) {
		var splitSlash = str.split("/");
		assert splitSlash.length == 2;

		var netId = splitSlash[0].split("\\.");
		assert netId.length == 4;

		int ip[] = new int[4];
		for (int i = 0; i < 4; ++i) {
			ip[i] = Integer.parseInt(netId[i]);
		}

		int netSize = Integer.parseInt(splitSlash[1]);
		return new IpV4Address(ip, netSize, 32 - netSize);
	}

	public static int[] llogaritNetworkMask(int subnetSize) {
		var arr = new int[]{
				Integer.MAX_VALUE & NR_MAGJIK,
				Integer.MAX_VALUE & NR_MAGJIK,
				Integer.MAX_VALUE & NR_MAGJIK,
				Integer.MAX_VALUE & NR_MAGJIK
		};

		int i;
		for (i = 3; subnetSize > 8; subnetSize -= 8, --i) {
			arr[i] = (arr[i] << 8) & NR_MAGJIK;
		}
		arr[i] = (arr[i] << subnetSize) & NR_MAGJIK;

		return arr;
	}

	public static long formatBinar(int ipv4[]) {
		long binar = 0;

		for (int i = 0; i < KUARTETE; ++i) {
			binar = binar << BITE_NE_BYTE;
			binar = binar | ipv4[i];
		}
		return binar;
	}

	public String raporti() {
		return String.format(
				"""
				Adresa IP: %s
				Network size: %d
				Subnet mask: %s
				Network ID: %s
				Adresa e pare e aksesueshme: %s
				Adresa e fundit e aksesueshme: %s
				Adresa e broadcastit: %s
				Numri i hosteve: %d
				""",
				Arrays.toString(adresaIp),
				networkSize,
				Arrays.toString(subnetMask),
				Arrays.toString(networkId),
				Arrays.toString(adresaPare),
				Arrays.toString(adresaFundit),
				Arrays.toString(broadcast),
				nrHostesh
		);
	}

	//
	// private
	//

	private int[] llogaritNetworkId() {
		var netId = new int[KUARTETE];

		for (int i = 0; i < KUARTETE; ++i) {
			netId[i] = adresaIp[i] & subnetMask[i];
		}
		return netId;
	}

	private int[] llogaritAdresaPare() {
		var addr = new int[KUARTETE];
		System.arraycopy(networkId, 0, addr, 0, KUARTETE);
		++addr[KUARTETE - 1];
		return addr;
	}

	private int[] llogaritBroadcast() {
		var oktetiFillimit = networkSize / BITE_NE_BYTE;
		var njeshiFunditMajtas = networkSize % BITE_NE_BYTE;
		var broadcast = new int[KUARTETE];
		System.arraycopy(networkId, 0, broadcast, 0, KUARTETE);

		broadcast[oktetiFillimit] = broadcast[oktetiFillimit] | (~(Integer.MAX_VALUE << (BITE_NE_BYTE - njeshiFunditMajtas)));
		for (int i = oktetiFillimit + 1; i < KUARTETE; ++i) {
			broadcast[i] = broadcast[i] | (Integer.MAX_VALUE & NR_MAGJIK);
		}

		return broadcast;
	}

	private int[] llogaritAdresaFundit() {
		var adresaFundit = new int[KUARTETE];
		System.arraycopy(broadcast, 0, adresaFundit, 0, KUARTETE);
		--adresaFundit[KUARTETE - 1];
		return adresaFundit;
	}

	private long llogaritNrHostesh() {
		return formatBinar(adresaFundit) - formatBinar(adresaPare) + 1;
	}
}
